import React from 'react';
import {
  Switch,
  Route
} from "react-router-dom";
import { AnimesPage } from '../../../pages/AnimesPage/AnimesPage';
import { ContactPage } from '../../../pages/ContactPage/ContactPage';
import { HomePage } from '../../../pages/HomePage/HomePage';

export function Routes(){
    return(
        <Switch>
            <Route exact path="/animes">
                <AnimesPage />
            </Route>
            <Route path="/contact">
                <ContactPage/>
            </Route>
            <Route path="/">
                <HomePage />
            </Route>
            <Route path="*">
                <h1>ERROR</h1>
            </Route>
        </Switch> 
    );
}