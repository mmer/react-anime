import React from 'react';
import { NavLink } from 'react-router-dom';

export function Menu() {
    return(
        <nav>
            <NavLink to="/">Home</NavLink>
            <NavLink to="/animes">Animes</NavLink>
            <NavLink to="/contact">Contact</NavLink>
        </nav>
    );
}