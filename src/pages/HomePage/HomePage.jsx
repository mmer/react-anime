import React from 'react';

export function HomePage(){

    return(
        <div>
        <h1>HOMEPAGE</h1>
        <span className="b-icon icon-beer-can"></span>
        <span className="icon-beer-tap"></span>
        <span className="icon-black-beer"></span>
        </div>
    );
}