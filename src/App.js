import './App.css';
import {
  BrowserRouter as Router
} from "react-router-dom";
import { Menu } from './core/components/Menu/Menu';
import { Routes } from './core/components/Routes/Routes';

function App() {
  return (
    <Router>
    <div className="App">
      <div className="App-header">
      <div className="container">
        <Menu></Menu>
        <Routes></Routes>
        </div>
      </div>
    </div>
    </Router>
  );
}

export default App;
